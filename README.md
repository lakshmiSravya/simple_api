# Simple API using Node.js

Creating Node Application

Node.js and the Express.js framework

## Installation

It requires Node.js v3+ to run.

Install the dependencies and devDependencies and start the server.


```
$ cd nodeApp
$ npm install -d
$ npm install express --save
$ npm install mongoose --save // creating database
$ npm install body-parser --save //express middleware
```

## Development

Open your Terminal and run these commands.


```
$ node app.js
```

