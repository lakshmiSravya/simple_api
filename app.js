
/* Simple API usinh Node.js and saving Data to MongoDB Database */
var express = require("express"); 
var app = express();
var port = 3000;
var mongoose = require("mongoose");
var url = "mongoose://localhost:27017/mydb"; 
var Schema = mongoose.Schema;
var nameSchema = new Schema({
	enterDate: String,
	_id: 0  
});
var User = mongoose.model("User", nameSchema);
var bodyParser = require('body-parser');
module.exports = User;


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.get("/", (req, res) => {
res.send("Hello World");
});


app.use("/hello", (req, res) => {
res.sendFile(__dirname + "/index.html");
});


app.post("/addname", (req, res) => {
  var myData = new User(req.body);
  myData.save()
    .then(item => {
      res.send("item saved to database");
    })
    .catch(err => {
      res.status(400).send("unable to save to database");
    });
});


app.listen(port, () => {
  console.log("Server listening on port " + port);
});

mongoose.Promise = global.Promise;
mongoose.connect("mongoose://localhost:27017/nodeApp");
